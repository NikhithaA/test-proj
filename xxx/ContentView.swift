//
//  ContentView.swift
//  xxx
//
//  Created by Nikhitha A on 2/13/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("HI Hello!")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
