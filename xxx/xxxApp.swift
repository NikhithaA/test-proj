//
//  xxxApp.swift
//  xxx
//
//  Created by Nikhitha A on 2/13/24.
//

import SwiftUI

@main
struct xxxApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
